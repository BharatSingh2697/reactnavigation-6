import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../Utils/NavigationConstants'

const DrawerOne = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>DRAWER ONE</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.DRAWER_STACK_ONE)}>
                <Text>GO TO DRAWER STACK ONE</Text>
            </TouchableOpacity>
        </View>
    )
}

export default DrawerOne

const styles = StyleSheet.create({})
