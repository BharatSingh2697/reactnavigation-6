import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const DrawerTwo = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>DRAWER TWO</Text>
        </View>
    )
}

export default DrawerTwo

const styles = StyleSheet.create({})
