import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const DrawerThree = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>DRAWER THREE</Text>
        </View>
    )
}

export default DrawerThree

const styles = StyleSheet.create({})
