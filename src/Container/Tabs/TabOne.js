import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../Utils/NavigationConstants'

const TabOne = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS TAB ONE</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.TAB_STACK_ONE)}>
                <Text>GO TO TAB STACK ONE</Text>
            </TouchableOpacity>
        </View>
    )
}

export default TabOne

const styles = StyleSheet.create({})
