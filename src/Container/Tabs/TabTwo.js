import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const TabTwo = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS TAB TWO</Text>
        </View>
    )
}

export default TabTwo

const styles = StyleSheet.create({})
