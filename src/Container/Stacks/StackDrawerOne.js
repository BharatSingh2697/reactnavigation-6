import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const StackDrawerOne = () => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>STACK DRAWER ONE</Text>
        </View>
    )
}

export default StackDrawerOne

const styles = StyleSheet.create({})
