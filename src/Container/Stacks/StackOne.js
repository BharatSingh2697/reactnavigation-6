import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const StackOne = () => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>STACK ONE</Text>
        </View>
    )
}

export default StackOne

const styles = StyleSheet.create({})
