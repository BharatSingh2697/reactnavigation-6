import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const StackTwo = () => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>STACK TWO</Text>
        </View>
    )
}

export default StackTwo

const styles = StyleSheet.create({})
