import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../../Utils/NavigationConstants'

const DrawerStackTwo = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS DRAWER STACK TWO</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.DRAWER_STACK_ONE)}>
                <Text>GO TO DRAWER STACK ONE</Text>
            </TouchableOpacity>
        </View>
    )
}

export default DrawerStackTwo

const styles = StyleSheet.create({})
