import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../../Utils/NavigationConstants'

const DrawerStackOne = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS DRAWER STACK ONE</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.DRAWER_STACK_TWO)}>
                <Text>GO TO DRAWER STACK TWO</Text>
            </TouchableOpacity>
        </View>
    )
}

export default DrawerStackOne

const styles = StyleSheet.create({})
