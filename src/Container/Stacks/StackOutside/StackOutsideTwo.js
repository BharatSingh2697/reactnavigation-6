import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../../Utils/NavigationConstants'

const StackOutsideTwo = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS STACK OUTSIDE TWO</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.STACK_OUTSIDE_ONE)}>
                <Text>GO TO STACK OUTSIDE ONE</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.TAB_NAV)}>
                <Text>GO TO TABS</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.DRAWER_NAV)}>
                <Text>GO TO DRAWER</Text>
            </TouchableOpacity>

        </View>
    )
}

export default StackOutsideTwo

const styles = StyleSheet.create({})
