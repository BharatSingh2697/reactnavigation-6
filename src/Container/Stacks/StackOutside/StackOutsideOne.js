import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../../Utils/NavigationConstants'
import AsyncStorage from '@react-native-async-storage/async-storage';

const StackOutsideOne = ({navigation}) => {

    const [name, setname] = useState("")
    const [pwd, setpassword] = useState("")

    const [auth, setauth] = useState(false)

    const loginHandle = () => {
        // setauth(true)
    }

    const registerHandle = async () => {
        // setauth(true)
        let data  = {
            name : name,
            password : pwd
        }
        let userData = JSON.stringify(data)
        try{
            AsyncStorage.setItem("user",userData)
        }
        catch(e){

        }
    }

    const fetchStorage = async() => {
        try {
            const value = await AsyncStorage.getItem("user")
            if(value!=null){
                console.log("USER : ",value);
            }else{
                console.log("HATT");
            }
        }
        catch(e){

        }
    }

    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS STACK OUTSIDE ONE</Text>

            {auth ? <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.STACK_OUTSIDE_TWO)}>
                <Text>GO TO STACK OUTSIDE TWO</Text>
            </TouchableOpacity>
            : <View style={{alignItems:'center'}}> 
            <View style={{marginTop:20}}>
            <TextInput
            value={name}
            onChangeText={(name=>setname(name))}
            style={styles.nameSt}
            placeholder={"NAME"}
            placeholderTextColor={"gray"}
            />
            <TextInput
            value={pwd}
            onChangeText={(pwd=>setpassword(pwd))}
            style={styles.nameSt}
            placeholder={"PASSWORD"}
            placeholderTextColor={"gray"}
            />
            </View>
            <TouchableOpacity style={styles.loginSt} onPress={()=>loginHandle()}>
                <Text style={{color:"white"}}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginSt} onPress={()=>registerHandle()}>
                <Text style={{color:"white"}}>Register</Text>
            </TouchableOpacity>
            </View>            
        }

            <TouchableOpacity style={{marginTop:50}} onPress={()=>fetchStorage()}>
            <Text>TESTING DATA</Text>
            </TouchableOpacity>

            
            
        </View>
    )
}

export default StackOutsideOne

const styles = StyleSheet.create({
    nameSt : {
        width:200,
        borderWidth:1,
        borderRadius:10,
        // height:30,
        marginTop:20,
        color:"black",
        paddingVertical:0,
        paddingHorizontal:20
    },
    loginSt:{
        marginTop:30,
        backgroundColor:'gray',
        borderRadius:30,
        paddingHorizontal:20,
        paddingVertical:7
    }
})
