import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../../Utils/NavigationConstants'

const TabStackOne = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS TAB STACK ONE</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.TAB_STACK_TWO)}>
                <Text>GO TO TAB STACK TWO</Text>
            </TouchableOpacity>
        </View>
    )
}

export default TabStackOne

const styles = StyleSheet.create({})
