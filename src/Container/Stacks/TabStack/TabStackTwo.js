import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { navigationConstants } from '../../../Utils/NavigationConstants'

const TabStackTwo = ({navigation}) => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>THIS IS TAB STACK TWO</Text>
            <TouchableOpacity style={{marginTop:20}} onPress={()=>navigation.navigate(navigationConstants.TAB_STACK_ONE)}>
                <Text>GO TO TAB STACK ONE</Text>
            </TouchableOpacity>
        </View>
    )
}

export default TabStackTwo

const styles = StyleSheet.create({})
