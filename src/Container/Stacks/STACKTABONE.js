import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const STACKTABONE = () => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>STACK TAB ONE</Text>
        </View>
    )
}

export default STACKTABONE

const styles = StyleSheet.create({})
