import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const StackThree = () => {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <Text>STACK THREE</Text>
        </View>
    )
}

export default StackThree

const styles = StyleSheet.create({})
