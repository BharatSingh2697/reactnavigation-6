import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { navigationConstants } from '../Utils/NavigationConstants';
import { createStackNavigator } from '@react-navigation/stack';
import NestingNavigator from './nestingNavigator';

const MainNavigation = () => {
    const Stack = createStackNavigator()
    return (
        <NavigationContainer>
            <NestingNavigator/>
        </NavigationContainer>
    )
}

export default MainNavigation