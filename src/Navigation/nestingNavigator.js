import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import  { navigationConstants }  from '../Utils/NavigationConstants';
import tabNavigator from './tabNavigator';
import StackOutsideOne from '../Container/Stacks/StackOutside/StackOutsideOne';
import StackOutsideTwo from '../Container/Stacks/StackOutside/StackOutsideTwo';
import drawerNavigation from './drawerNavigator';

const nestingNavigator = () => {
    const NestingNavigator = createStackNavigator()
    return (
        <NestingNavigator.Navigator>
            <NestingNavigator.Screen name={navigationConstants.STACK_OUTSIDE_ONE} component={StackOutsideOne}/>
            <NestingNavigator.Screen name={navigationConstants.STACK_OUTSIDE_TWO} component={StackOutsideTwo}/>
            <NestingNavigator.Screen name={navigationConstants.TAB_NAV} component={tabNavigator}/>
            <NestingNavigator.Screen name={navigationConstants.DRAWER_NAV} component={drawerNavigation}/>
        </NestingNavigator.Navigator>
    )
}

export default nestingNavigator