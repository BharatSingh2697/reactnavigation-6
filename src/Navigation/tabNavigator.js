import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { navigationConstants } from '../Utils/NavigationConstants';
import TabOne from '../Container/Tabs/TabOne';
import TabTwo from '../Container/Tabs/TabTwo';
import TabThree from '../Container/Tabs/TabThree';
import tabStackNavigator from './tabStackNavigator';


const tabNavigator = () => {
    const Tab = createBottomTabNavigator()
    return (
        <Tab.Navigator >
           <Tab.Screen name={navigationConstants.TAB_STACK_NAV} component={tabStackNavigator} />
           <Tab.Screen name={navigationConstants.TAB_TWO} component={TabTwo} />
           <Tab.Screen name={navigationConstants.TAB_THREE} component={TabThree} />
        </Tab.Navigator>
    )
}

export default tabNavigator