import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { navigationConstants } from '../Utils/NavigationConstants';
import DrawerOne from '../Container/Drawer/DrawerOne';
import DrawerTwo from '../Container/Drawer/DrawerTwo';
import DrawerThree from '../Container/Drawer/DrawerThree';
import drawerStackNavigator from '../Navigation/drawerStackNavigator'


const drawerNavigation = ({navigation}) => {
    const Drawer = createDrawerNavigator()
    return (
        <Drawer.Navigator >
            <Drawer.Screen name={navigationConstants.DRAWER_STACK_NAV} component={drawerStackNavigator}/>
            <Drawer.Screen name={navigationConstants.DRAWER_TWO} component={DrawerTwo}/>
            <Drawer.Screen name={navigationConstants.DRAWER_THREE} component={DrawerThree}/>
        </Drawer.Navigator>
    )
}

export default drawerNavigation