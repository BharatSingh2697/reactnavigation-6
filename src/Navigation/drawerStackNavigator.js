import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import  { navigationConstants }  from '../Utils/NavigationConstants';
import DrawerOne from '../Container/Drawer/DrawerOne';
import DrawerStackOne from '../Container/Stacks/DrawerStack/DrawerStackOne';
import DrawerStackTwo from '../Container/Stacks/DrawerStack/DrawerStackTwo';

const drawerStackNavigator = () => {
    const Stack = createStackNavigator()
    return (
        <Stack.Navigator>
             <Stack.Screen name={navigationConstants.DRAWER_ONE} component={DrawerOne} />
             <Stack.Screen name={navigationConstants.DRAWER_STACK_ONE} component={DrawerStackOne}/>
             <Stack.Screen name={navigationConstants.DRAWER_STACK_TWO} component={DrawerStackTwo}/>
        </Stack.Navigator>
    )
}

export default drawerStackNavigator