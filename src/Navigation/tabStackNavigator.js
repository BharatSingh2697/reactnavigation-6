import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import  { navigationConstants }  from '../Utils/NavigationConstants';
import StackOne from '../Container/Stacks/StackOne';
import TabStackOne from '../Container/Stacks/TabStack/TabStackOne';
import TabStackTwo from '../Container/Stacks/TabStack/TabStackTwo';
import TabOne from '../Container/Tabs/TabOne';

const tabStackNavigator = () => {
    const Stack = createStackNavigator()
    return (
        <Stack.Navigator>
             <Stack.Screen name={navigationConstants.TAB_ONE} component={TabOne} />
             <Stack.Screen name={navigationConstants.TAB_STACK_ONE} component={TabStackOne}/>
             <Stack.Screen name={navigationConstants.TAB_STACK_TWO} component={TabStackTwo}/>
        </Stack.Navigator>
    )
}

export default tabStackNavigator